$(document).ready(function(){
			$(".default_option").click(function(){
			  $(".dropdown ul").addClass("active");
			});

			$(".dropdown ul li").click(function(){
			  var text = $(this).text();
			  $(".default_option").text(text);
			  $(".dropdown ul").removeClass("active");
			});
           
});


$(document).ready(function() {
  $("#example").DataTable({
    aaSorting: [],
    responsive: true,

    columnDefs: [
      {
        responsivePriority: 1,
        targets: 0
      },
      {
        responsivePriority: 2,
        targets: -1
      }
    ]
  });

  $(".dataTables_filter input")
    .attr("placeholder", "Search here...")
    .css({
      width: "300px",
      display: "inline-block"
    });

  $('[data-toggle="tooltip"]').tooltip();
});


