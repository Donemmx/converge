// chart One Demo


const options = {
          series: [44, 55],
          chart: {
          width: 380,
          type: 'donut',
        },

        dataLabels: {
          enabled: false
        },

        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 260
            },
            legend: {
              show: true,
              offsetY: 30,

            }
          }
        }],

        legend: {
          position: 'right',
          offsetY: 60,
          height: 80,
        }
};


const chart = new ApexCharts(document.querySelector("#chart-1"), options);
     



// render Chart 
   chart.render();


// chart 2

     const options2 = {
          series: [67],
          chart: {
          width: 180,
          type: 'radialBar',
          
          
        },
        plotOptions: {
        
            
          radialBar: {
         
            hollow: {
            //   margin: 15,
            margin: 0,
              size: '70%',
              background: '#FFEFCB',
             
            },

        track: {
              background: '#FFEFCB',
              strokeWidth: '67%',
              margin: 0, // margin is in pixels
         
            },
        

            dataLabels: {
                
              name: {
                show: false,
                fontSize: '12px',
                color: '#FFB200',
                
                
                
              },

              value: {
                show: true,
                offsetY: 10,
                fontSize: '16px',
                formatter: function(val) {
                  return parseInt(val);
                },
                color: '#111',
                
              }
            
            }
            
          }
          
        },

        fill: {
         colors: ['#FFB200']
        },


        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 150,
                
            },
            legend: {
              show: false,
              offsetY: 30,
            

            }
          }
        }],
   
        stroke: {
          lineCap: 'round',
         
          
        },
        

        
        //   title: {
        //             text: 'Finance',
        //               style: {
        //           fontSize: '14px',
        //           textAlign: 'center',
        //           color: '#FFB200'
        //       }
        //         },
       
        };

        const chart2 = new ApexCharts(document.querySelector("#chart-2"), options2);
        chart2.render();

        // end of chart 2



// chart 3

     const options3 = {
          series: [46],
          chart: {
          width: 180,
          type: 'radialBar',
        },
        plotOptions: {
          radialBar: {
            hollow: {
           margin: 0,
              size: '70%',
              background: '#DAD7FE',

            },


            dataLabels: {
              name: {
                show: false,
                color: '#fff'
              },

             value: {
                show: true,
                offsetY: 10,
                fontSize: '16px',
                formatter: function(val) {
                  return parseInt(val);
                },
                color: '#111',
                
              },
             
            },

                 track: {
              background: '#DAD7FE',
              strokeWidth: '67%',
              margin: 0, // margin is in pixels
         
            },

          }
        },

          fill: {
         colors: ['#4339F2']
        },


        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 150
            },
            legend: {
              show: false,
              offsetY: 30,

            }
          }
        }],
   
        stroke: {
          lineCap: 'round',
          
        },
        //  title: {
        //             text: 'Finance',
        //           style: {
        //           fontSize: '14px',
        //           textAlign: 'center',
        //           color: '#4339F2'
        //       }

        //         },
        // labels: ['Volatility'],
        };

        const chart3 = new ApexCharts(document.querySelector("#chart-3"), options3);
        chart3.render();

        // end of chart 2




// chart 4

     const options4 = {
          series: [15],
          chart: {
          width: 180,
          type: 'radialBar',
        },
        plotOptions: {
          radialBar: {
            hollow: {
           margin: 0,
              size: '70%',
              background: '#FFE5D3',

            },


            dataLabels: {
              name: {
                show: false,
                color: '#fff'
              },

             value: {
                show: true,
                offsetY: 10,
                fontSize: '16px',
                formatter: function(val) {
                  return parseInt(val);
                },
                color: '#111',
                
              },
             
            },

                 track: {
              background: '#FFE5D3',
              strokeWidth: '67%',
              margin: 0, // margin is in pixels
         
            },

          }
        },

          fill: {
         colors: ['#FF3A29']
        },


        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 150
            },
            legend: {
              show: false,
              offsetY: 30,

            }
          }
        }],
   
        stroke: {
          lineCap: 'round',
          
        },
        //  title: {
        //             text: 'Finance',
        //           style: {
        //           fontSize: '14px',
        //           textAlign: 'center',
        //           color: '#4339F2'
        //       }

        //         },
        // labels: ['Volatility'],
        };

        const chart4 = new ApexCharts(document.querySelector("#chart-4"), options4);
        chart4.render();

        // end of chart 4


// chart 5

     const options5 = {
          series: [67],
          chart: {
          width: 180,
          type: 'radialBar',
        },
        plotOptions: {
          radialBar: {
            hollow: {
           margin: 0,
              size: '70%',
              background: '#CCF8FE',

            },


            dataLabels: {
              name: {
                show: false,
                color: '#fff'
              },

             value: {
                show: true,
                offsetY: 10,
                fontSize: '16px',
                formatter: function(val) {
                  return parseInt(val);
                },
                color: '#111',
                
              },
             
            },

                 track: {
              background: '#CCF8FE',
              strokeWidth: '67%',
              margin: 0, // margin is in pixels
         
            },

          }
        },

          fill: {
         colors: ['#02A0FC']
        },


        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 150
            },
            legend: {
              show: false,
              offsetY: 30,

            }
          }
        }],
   
        stroke: {
          lineCap: 'round',
         
        },
        //  title: {
        //             text: 'Finance',
        //           style: {
        //           fontSize: '14px',
        //           textAlign: 'center',
        //           color: '#4339F2'
        //       }

        //         },
        };

        const chart5 = new ApexCharts(document.querySelector("#chart-5"), options5);
        chart5.render();

        // end of chart 5




// chart 6

     const options6 = {
          series: [67],
          chart: {
          width: 150,
          type: 'radialBar',
        },
        plotOptions: {
          radialBar: {
            hollow: {
           margin: 0,
              size: '70%',
              background: '#fff',

            },


            dataLabels: {
              name: {
                show: false,
                color: '#fff'
              },

             value: {
                show: true,
                offsetY: 10,
                fontSize: '18px',
                formatter: function(val) {
                  return parseInt(val);
                },
                color: '#111',
                
              },
             
            },

                 track: {
              background: '#efefef',
              strokeWidth: '67%',
              margin: 0, // margin is in pixels
         
            },

          }
        },

          fill: {
         colors: ['#79D2DE']
        },


        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 130
            },
            legend: {
              show: false,
              offsetY: 30,

            }
          }
        }],
   
        stroke: {
          lineCap: 'round',
         
        },
        //  title: {
        //             text: 'Finance',
        //           style: {
        //           fontSize: '14px',
        //           textAlign: 'center',
        //           color: '#4339F2'
        //       }

        //         },
        };

        const chart6 = new ApexCharts(document.querySelector("#chart-6"), options6);
        chart6.render();

        // end of chart 6



// chart 7

     const options7 = {
          series: [67],
          chart: {
          width: 150,
          type: 'radialBar',
        },
        plotOptions: {
          radialBar: {
            hollow: {
           margin: 0,
              size: '70%',
              background: '#fff',

            },


            dataLabels: {
              name: {
                show: false,
                color: '#fff'
              },

             value: {
                show: true,
                offsetY: 10,
                fontSize: '18px',
                formatter: function(val) {
                  return parseInt(val);
                },
                color: '#111',
                
              },
             
            },

                 track: {
              background: '#efefef',
              strokeWidth: '67%',
              margin: 0, // margin is in pixels
         
            },

          }
        },

          fill: {
         colors: ['#79D2DE']
        },


        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 130
            },
            legend: {
              show: false,
              offsetY: 30,

            }
          }
        }],
   
        stroke: {
          lineCap: 'round',
         
        },
        //  title: {
        //             text: 'Finance',
        //           style: {
        //           fontSize: '14px',
        //           textAlign: 'center',
        //           color: '#4339F2'
        //       }

        //         },
        };

        const chart7 = new ApexCharts(document.querySelector("#chart-7"), options7);
        chart7.render();

        // end of chart 7


        // Chart 8

         const options8 = {
          series: [{
          name: 'Contract Staff',
          data: [44, 55, 41, 67, 22, 43]
        }, {
          name: 'Full staff',
          data: [13, 23, 20, 8, 13, 27]
        }],
          chart: {
          type: 'bar',
          height: 350,
          stacked: true,
          toolbar: {
            show: false
          },
          zoom: {
            enabled: false
          }
        },
        responsive: [{
          breakpoint: 480,
          options: {
            legend: {
              position: 'bottom',
              offsetX: -10,
              offsetY: 0
            }
          }
        }],
        plotOptions: {
          bar: {
            borderRadius: 8,
            horizontal: true,
            barWidth: '20%', 
          },
        },
        xaxis: {
          type: 'month',
          categories: ['jan', 'feb', 'mar', 'apr',
            'may', 'jun'
          ],
        },
        legend: {
          position: 'right',
          offsetY: 40
        },
        fill: {
          opacity: 1
        }
        };

        const chart8 = new ApexCharts(document.querySelector("#chart-8"), options8);
        chart8.render();
        // End of Chart 8